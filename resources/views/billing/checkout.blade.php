  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <x-app-layout>
      <x-slot name="header">
          <h2 class="font-semibold text-xl text-gray-800 leading-tight">
              Subscribe to {{ $plan->name }}
          </h2>
      </x-slot>

      <div class="py-12">
          <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
              <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                  <div class="p-6 bg-white border-b border-gray-200">
                    @if(session('error'))
                    <div class="alert alert-danger">{{session('error')}}</div>
                    @endif
                      <form action={{ route('checkout.process') }} method="POST" id="checkout-form">
                          @csrf
                          <input type="hidden" name="billing_plan_id" name="billing_plan_id"
                              value="{{ $plan->id }}" />
                          <input type="hidden" name="payment-method" name="payment-method" value="visa" />
                          <input id="card-holder-name" type="text">

                          <!-- Stripe Elements Placeholder -->
                          <div id="card-element"></div>
                          <br />
                          <button id="card-button" class="btn btn-primary">
                              Pay ${{ number_format($plan->price / 100, 2) }}
                          </button>

                      </form>
                  </div>
              </div>
          </div>
      </div>
  </x-app-layout>
  <script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
    crossorigin="anonymous"></script>
  {{-- @section('scripts') --}}
  <script src="https://js.stripe.com/v3/"></script>
  <script>
      $(document).ready(function() {
          let stripe = Stripe("{{ env('STRIPE_KEY') }}")
          let elements = stripe.elements()
          let style = {
              base: {
                  color: '#32325d',
                  fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                  fontSmoothing: 'antialiased',
                  fontSize: '16px',
                  '::placeholder': {
                      color: '#aab7c4'
                  }
              },
              invalid: {
                  color: '#fa755a',
                  iconColor: '#fa755a'
              }
          }
          let card = elements.create('card', {
              style: style
          })
          card.mount('#card-element')
          let paymentMethod = null
          $('#checkout-form').on('submit', function(e) {
              $('#card-button').prop('disabled', true);
              if (paymentMethod) {
                  return true
              }
              var card_holder_name = $('#card-holder-name').val();
              if (card_holder_name == null || card_holder_name == "" || card_holder_name == undefined) {
                  return false
              }
              stripe.confirmCardSetup(
                  "{{ $intent->client_secret }}", {
                      payment_method: {
                          card: card,
                          billing_details: {
                              name: card_holder_name
                          }
                      }
                  }
              ).then(function(result) {
                  if (result.error) {
                      console.log(result)
                      alert('error')
                  } else {
                      paymentMethod = result.setupIntent.payment_method

                      $('#payment-method').val(paymentMethod)
                      $('#checkout-form').submit()
                  }
              })
              return false
          })

      });
  </script>
  {{-- @endsection --}}
