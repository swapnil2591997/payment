  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Billing') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <div class="card-body">
                        <div class="row">
                            @foreach($plans as $plan)
                                <div class="col-md-4 text-center">
                                <h1>{{$plan->name}} </h1>
                                <b>${{$plan->price}}</b>
                                <hr />
                                <a href="{{route('checkout',$plan->id)}}" class="btn btn-primary">Subscribe to {{$plan->name}}</a>
                            </div>
                            @endforeach
                     
                              {{--  <div class="col-md-4 text-center">
                                <h1>Bronze Plan </h1>
                                <b>$9.99</b>
                                <hr />
                                <a href="#" class="btn btn-primary">Subscribe to bronze</a>
                            </div>
                              <div class="col-md-4 text-center">
                                <h1>Bronze Plan </h1>
                                <b>$9.99</b>
                                <hr />
                                <a href="#" class="btn btn-primary">Subscribe to bronze</a>
                            </div>  --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
