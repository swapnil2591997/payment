<?php

namespace App\Http\Controllers;

use App\Models\Plan;
use Illuminate\Http\Request;

class BillingController extends Controller
{
    public function index()
    {
        $plans = Plan::all();
        return view('billing.index', compact('plans'));
    }

    public function checkout($id)
    {
        $plan   = Plan::findOrfail($id);
        $intent = auth()->user()->createSetupIntent();
        return view('billing.checkout', compact('plan', 'intent'));
    }

    public function ProcessCheckout(Request $request)
    {
        $plan = Plan::findorfail($request->input('billing_plan_id'));

        try {
            auth()->user()->newSubscription(
                $plan,$plan->stripe_id
            )->create();
            return redirect('billing')->withMessage('Subscribed Successfully');
        } catch (\Exception $e) {
            return redirect()->back()->withError($e->getMessage());

        }
    }
}
