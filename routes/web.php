<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BillingController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';
Route::get('billing',[BillingController::class,'index'])->name('billing');
Route::get('checkout/{id}',[BillingController::class,'checkout'])->name('checkout');
Route::post('checkout',[BillingController::class,'ProcessCheckout'])->name('checkout.process');